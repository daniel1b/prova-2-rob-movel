function vetor = rota_aestr(mapaABS,pos_inicial,pos_final,menor_certo1)
menor_certo = menor_certo1;
pai = [pos_inicial(1) pos_inicial(2)];
caminho = {pai};
linha = length(mapaABS(:,1));
coluna = length(mapaABS(1,:));
tamanho_min = abs(pai(1)-pos_final(1)+1)+abs(pai(2)-pos_final(2));
while (pai(1) ~= pos_final(1))||(pai(2) ~= pos_final(2))
    cla;
    mapaABS(pai(1),pai(2))=3;
    desenha_mapa(mapaABS);
    hold on
    i = pos_final(1);
    j = pos_final(2);
    patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 0]);
    i = pos_inicial(1);
    j = pos_inicial(2);
    patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 1]);
    hold off
    pause(0.002)
    %----------------------------------x-------Y---------G---------------------H-------------------------------ANT
    A ={};
    if pai(1)+1<=linha
        if mapaABS(pai(1)+1,pai(2))==1;
            A{length(A)+1} = [pai(1)+1 pai(2) abs(pai(1)-pos_final(1)+1)+abs(pai(2)-pos_final(2))];
        end
    end
    if pai(1)-1>0
        if mapaABS(pai(1)-1,pai(2))==1;
            A{length(A)+1} = [pai(1)-1 pai(2) abs(pai(1)-pos_final(1)-1)+abs(pai(2)-pos_final(2))];
        end
    end
    if pai(2)+1<=coluna
        if mapaABS(pai(1),pai(2)+1)==1;
            A{length(A)+1} = [pai(1) pai(2)+1 abs(pai(1)-pos_final(1))+abs(pai(2)-pos_final(2)+1)];
        end
    end
    if pai(2)-1>0
        if mapaABS(pai(1),pai(2)-1)==1;
            A{length(A)+1} = [pai(1) pai(2)-1 abs(pai(1)-pos_final(1))+abs(pai(2)-pos_final(2)-1)];
        end
    end
    if ~isempty(A)
        Menor = A{1};
        cm_parcial = {};
        for mn = 1:length(A)
            if Menor(3)>A{mn}(3)
                Menor=A{mn};
            elseif Menor(3)==A{mn}(3)
                if length(caminho)>=2
                    if (A{mn}(1)-caminho{length(caminho)}(1)) == (caminho{length(caminho)}(1)-caminho{length(caminho)-1}(1))&&...
                            (A{mn}(2)-caminho{length(caminho)}(2)) == (caminho{length(caminho)}(2)-caminho{length(caminho)-1}(2))
                        Menor=A{mn};
                    end
                end
            end
        end
        contador = 0;
        for mn = 1:length(A)
            if Menor(3)==A{mn}(3)
                contador= contador+1;
            end
        end
        if contador>1&&length(A)<5
            for mn = 1:length(A)
                if Menor(3)==A{mn}(3)
                    if menor_certo==0
                        x1 = rota_aestr(mapaABS,[A{mn}(1),A{mn}(2)],pos_final,0);
                    else
                        x1 = rota_aestr(mapaABS,[A{mn}(1),A{mn}(2)],pos_final,menor_certo);
                    end
                    if ~isempty(x1)
                        if length(x1)<menor_certo||menor_certo==0
                            menor_certo = length(x1);
                            cm_parcial {length(cm_parcial)+1} = x1;
                        end
                    end
                end
            end
            if ~isempty(cm_parcial)
                tam = length(cm_parcial{1});
                for jj = 1:length(cm_parcial)
                    if tam>length(cm_parcial{jj})&&~isempty(cm_parcial{jj})
                        tam = length(cm_parcial{jj});
                    end
                end
                for jj = 1:length(cm_parcial)
                    if tam==length(cm_parcial{jj})
                        caminho = [caminho cm_parcial{jj}];
                        break;
                    end
                end
            else
                pai = pos_final;
                caminho = {};
            end
            break;
        else
            pai = [Menor(1),Menor(2)];
            caminho{length(caminho)+1} = pai;
            if length(caminho)>menor_certo1&&menor_certo1~=0
                pai = pos_final;
                caminho = {};
                break;
            end
        end
    else
        pai = pos_final;
        caminho = {};
    end
end
vetor = caminho;
end