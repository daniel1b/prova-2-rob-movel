function desenha_robo(xy,orient)
hold on
i = xy(1)-1;
j = xy(2)-1;

x = [j*10+2.5 j*10+7.5 j*10+7.5 j*10+2.5];
y = [i*10+2.5 i*10+2.5 i*10+7.5 i*10+7.5];
patch(x,y,[0 0 1])

x = [j*10+5 j*10+2.5 j*10+7.5];
y = [i*10+5 i*10+7.5 i*10+7.5];
[x,y] = rot_esp(x,y,orient-pi/2,j*10+5,i*10+5) ;
patch(x,y,[1 0 0])

hold off;
end