function desenha_seta(xy)
figure(1);
hold on
for k = 2:length(xy)
    i = xy{k}{1}(1)-1;
    j = xy{k}{1}(2)-1;
    x = [j*10+2 j*10+6 j*10+6 j*10+8 j*10+6 j*10+6 j*10+2];
    y = [i*10+6.5 i*10+6.5 i*10+9 i*10+5 i*10+1 i*10+3.5 i*10+3.5];
    [x,y] = rot_esp(x,y,xy{k}{2},j*10+5,i*10+5) ;
    patch(x,y,[1 0 0])
end
hold off;
end