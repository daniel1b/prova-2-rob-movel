clc; clear all;cla
mapaABS = zeros(12,12);
%0 para descconhecido
%1 para vazio
%2 para obstaculo
prob_obstaculo = 0.2;

%cria os obstaculos no mapa
for i = 1:length(mapaABS(:,1))
    for j = 1:length(mapaABS(1,:))
        if rand()>prob_obstaculo
            mapaABS(i,j) = 1;%livre
        else
            mapaABS(i,j) = 2;%obstaculo
        end
    end
end
for d = 1:length(mapaABS(:,1))
    mapaABS(d,1) = 2;
    mapaABS(d,length(mapaABS(1,:))) = 2;
end
for d = 1:length(mapaABS(1,:))
    mapaABS(1,d) = 2;
    mapaABS(length(mapaABS(:,1)),d) = 2;
end

mapadesc = mapaABS;
for i = 2:length(mapadesc(:,1))-1
    for j = 2:length(mapadesc(1,:))-1
        mapadesc(i,j) = 0;
    end
end

%posicao inicial
pos_inicial = [2,2];
mapaABS(2,2) = 2;

figure(1)
cla

desenha_mapa(mapaABS);
hold on
i = pos_inicial(1);
j = pos_inicial(2);
patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 0]);
hold off
mapadesc(2,2) = 1;
robo = robo_class(pos_inicial,0,mapadesc);
robo = robo.girar_D(mapaABS);
robo = robo.girar_E(mapaABS);
robo.desenha_robo();

for linha = 2:length(mapaABS(:,1))-1
    [ha_cinza,destino] = robo.verifica_linha(linha);
    while ha_cinza
%         robo = robo.girar_D(mapaABS);
%         robo = robo.girar_D(mapaABS);
%         robo = robo.girar_D(mapaABS);
%         robo = robo.girar_D(mapaABS);
        disp('aperte')
        
        pos_inicial = robo.xy_rel;
        vetor = rota_aestr_v2(robo.mapa,robo.xy_rel,destino,100,0);
        for d =1:length(vetor)
            i = vetor{d}(1);
            j = vetor{d}(2);
            patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 0 1]);
        end
        %waitforbuttonpress 
        deltat = 0.3;
        for i = 2:length(vetor)
            girar = robo.orient;
            count = 0;
            cel = [vetor{i-1}(1)+round(sin(girar)) vetor{i-1}(2)+round(cos(girar))];
            while cel(1)~=vetor{i}(1)||cel(2)~=vetor{i}(2)
                count = count+1;
                girar = girar + pi/2;
                cel = [vetor{i-1}(1)+round(sin(girar)) vetor{i-1}(2)+round(cos(girar))];
            end
            
            
            if count == 1
                robo = robo.girar_E(mapaABS);
                robo.desenha_robo();
                pause(deltat)
            elseif count == 2
                robo = robo.girar_D(mapaABS);
                robo.desenha_robo();
                pause(deltat)
                robo = robo.girar_D(mapaABS);
                robo.desenha_robo();
                pause(deltat)
            elseif count == 3
                robo = robo.girar_D(mapaABS);
                robo.desenha_robo();
                pause(deltat)
            end
            p_ant = robo.xy;
            robo = robo.Mover_F(mapaABS);
            
            cla
            desenha_mapa(mapaABS);
            hold on
            k = destino(1);
            j = destino(2);
            patch([(j-1)*10 j*10 j*10 (j-1)*10],[(k-1)*10 (k-1)*10 k*10 k*10], [0 1 0]);
            k = pos_inicial(1);
            j = pos_inicial(2);
            patch([(j-1)*10 j*10 j*10 (j-1)*10],[(k-1)*10 (k-1)*10 k*10 k*10], [1 0 1]);
            hold off
            robo.desenha_robo();
            
            if robo.xy(1)==p_ant(1)&&robo.xy(2)==p_ant(2)
                break;
            end
            pause(deltat)
        end
        
        [ha_cinza,destino] = robo.verifica_linha(linha);
    end
end




