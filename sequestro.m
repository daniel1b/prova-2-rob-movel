clc; clear all;cla 
mapaABS = zeros(12,12); 
%0 para descconhecido
%1 para vazio
%2 para obstaculo
prob_obstaculo = 0.1;

%cria os obstaculos no mapa
for i = 1:length(mapaABS(:,1))
    for j = 1:length(mapaABS(1,:))
        if rand()>prob_obstaculo
            mapaABS(i,j) = 1;%livre
        else
            mapaABS(i,j) = 2;%obstaculo
        end
    end
end
for d = 1:length(mapaABS(:,1))
    mapaABS(d,1) = 2;
    mapaABS(d,length(mapaABS(1,:))) = 2;
end
for d = 1:length(mapaABS(1,:))
    mapaABS(1,d) = 2;
    mapaABS(length(mapaABS(:,1)),d) = 2;
end

%coloca o robo numa posicao aleatoria onde nao tem obstaculos
robo = robo_class([randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)],randi([0 3],1,1)*pi/2);
while mapaABS(robo.xy(1),robo.xy(2))==2
    robo.xy = [randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)];
end



figure(1)
desenha_mapa(mapaABS);
robo.desenha_robo();
