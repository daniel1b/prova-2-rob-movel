function desenha_mapa(matriz)
linha = length(matriz(:,1));
coluna = length(matriz(1,:));

hold on
%         cinza      branco preto      azul
cores = {[.5 .5 .5],[1 1 1],[0 0 0],[1 0 0]};
for i = 1:linha
    for j = 1:coluna
       patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], cores{matriz(i,j)+1});  
    end
end

%desenha as linhas
for i = 0:coluna
    patch([(i*10 - 0.5) (i*10 + 0.5) (i*10 + 0.5) (i*10 - 0.5)],[0 0 linha*10 linha*10], [0 0 0]);
end
for i = 0:linha
    patch([0 0 coluna*10 coluna*10],[(i*10 - 0.5) (i*10 + 0.5) (i*10 + 0.5) (i*10 - 0.5)], [0 0 0]);
end

hold off
if coluna>linha
    xlim([-5 coluna*10+5]);
    ylim([-5 coluna*10+5]);
else
    xlim([-5 linha*10+5]);
    ylim([-5 linha*10+5]);
end
end



