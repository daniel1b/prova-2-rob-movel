function arr = comparaMapas(mapaABS,mapaREL,pivo)
arr = {0};
for k = 0:3
    if k ~= 0
        [mapaREL,pivo] = roda_Mat(mapaREL,pivo);
    end
    
    %"tirar" os zeros da relativa,ja que eles nao importam
    iniciol = 1;
    iniciom = 1;
    
    for l = 1:length(mapaREL(:,1))
        bol =1;
        for m = 1:length(mapaREL(1,:))
            if mapaREL(l,m) ~= 0
                bol = 0;
            end
        end
        if bol
            iniciol = l+1;
        else
            break;
        end
    end
    
    for m = 1:length(mapaREL(1,:))
        bol =1;
        for l = 1:length(mapaREL(:,1))
            if mapaREL(l,m) ~= 0
                bol = 0;
            end
        end
        if bol
            iniciom =m+1;
        else
            break;
        end
    end
    
    
    %comecar a busca
    for i = 1-iniciol:length(mapaABS(:,1))
        for j = 1-iniciom:length(mapaABS(1,:))
            boleano = true;
            cont = 0;
            for l = iniciol:length(mapaREL(:,1))
                for m = iniciom:length(mapaREL(1,:))
                    if and((i+l)<= length(mapaABS(:,1)),(j+m)<= length(mapaABS(1,:)))
                        if(mapaREL(l,m)~=0)
                            if(mapaREL(l,m)~=mapaABS(i+l,j+m))
                                boleano = false;
                            else
                                cont = cont+1;
                            end
                        end
                    else
                        boleano = false;
                        break
                    end
                end
                if not(boleano)
                    break;
                end
            end
            if boleano
                arr{length(arr)+1} = {[pivo(1)+i pivo(2)+j] k*pi/2};
            end
        end
    end
end
end