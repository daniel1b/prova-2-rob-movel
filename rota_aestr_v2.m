function vetor = rota_aestr_v2(mapaABS,pos_inicial,pos_final,menor_certo,bol)
%======================================================================
caminho = {pos_inicial};
pai = pos_inicial;
linha = length(mapaABS(:,1));
coluna = length(mapaABS(1,:));
if ceil(sqrt((pos_inicial(1)-pos_final(1))^2 + (pos_inicial(2)-pos_final(2))^2))==1;
    caminho = {pos_inicial pos_final};
else
    while pai(1)~=pos_final(1)||pai(2)~=pos_final(2)
        mapaABS(pai(1),pai(2))=3;
        %============desenhar o mapa e o caminho===============================
        cla;
        desenha_mapa(mapaABS);
        hold on
        i = pos_final(1);
        j = pos_final(2);
        patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 0]);
        i = pos_inicial(1);
        j = pos_inicial(2);
        patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 1]);
        hold off
        pause(0.01);
        %============desenhar o mapa e o caminho===============================
        %======================================================================
        %============possiveis caminhos========================================
        A ={};
        if pai(1)+1<=linha
            if mapaABS(pai(1)+1,pai(2))==0||mapaABS(pai(1)+1,pai(2))==1
                A{length(A)+1} = [pai(1)+1 pai(2) abs(pai(1)-pos_final(1)+1)+abs(pai(2)-pos_final(2))];
            end
        end
        if pai(1)-1>0
            if mapaABS(pai(1)-1,pai(2))==0||mapaABS(pai(1)-1,pai(2))==1
                A{length(A)+1} = [pai(1)-1 pai(2) abs(pai(1)-pos_final(1)-1)+abs(pai(2)-pos_final(2))];
            end
        end
        if pai(2)+1<=coluna
            if mapaABS(pai(1),pai(2)+1)==0||mapaABS(pai(1),pai(2)+1)==1
                A{length(A)+1} = [pai(1) pai(2)+1 abs(pai(1)-pos_final(1))+abs(pai(2)-pos_final(2)+1)];
            end
        end
        if pai(2)-1>0
            if mapaABS(pai(1),pai(2)-1)==0||mapaABS(pai(1),pai(2)-1)==1
                A{length(A)+1} = [pai(1) pai(2)-1 abs(pai(1)-pos_final(1))+abs(pai(2)-pos_final(2)-1)];
            end
        end
        %======================================================================
        %==========================Avaliar caminhos============================
        if isempty(A) %%===========desistir desse caminho,nao leva a nada======
            caminho = {};
            break;
        else
            Melhor_aval = A{1};
            for cnt = 1:length(A)
                if Melhor_aval(3)>A{cnt}(3)
                    Melhor_aval = A{cnt}
                end
            end
            %waitforbuttonpress
            busca = {};
            for cnt = 1:length(A)
                if Melhor_aval(3)==A{cnt}(3)
                    busca{length(busca)+1} = A{cnt};
                end
            end
            if length(busca)==1 % ja adicionar ao caminho de vez
                pai = [busca{1}(1),busca{1}(2)];
                caminho = [caminho pai];
                if length(caminho)>menor_certo&&bol
                    caminho = {};
                    break;
                end
            else
                opc_1 = rota_aestr_v2(mapaABS,[busca{1}(1),busca{1}(2)],pos_final,menor_certo-length(caminho),1);
                if isempty(opc_1)
                    opc_2 = rota_aestr_v2(mapaABS,[busca{2}(1),busca{2}(2)],pos_final,menor_certo-length(caminho),1);
                    if isempty (opc_2)
                        caminho = {};
                    else
                        caminho = [caminho opc_2];
                    end
                else
                    length(opc_1)
                    opc_2 = rota_aestr_v2(mapaABS,[busca{2}(1),busca{2}(2)],pos_final,length(opc_1),1);
                    if isempty (opc_2)
                        caminho = [caminho opc_1];
                    else
                        caminho = [caminho opc_2];
                    end
                end
                
                break;
            end
        end
    end
end
vetor = caminho;

end