function [saida,pivot] = roda_Mat(Mat,pivo)
saida = zeros(length(Mat(1,:)),length(Mat(:,1)));
for i = 1:length(Mat(:,1))
    for j = 1:length(Mat(1,:))
        saida(j,length(Mat(:,1))-i+1) = Mat(i,j);
        if and(i==pivo(1),j==pivo(2))
            pivot = [j,length(Mat(:,1))-i+1];
        end
    end
end
end