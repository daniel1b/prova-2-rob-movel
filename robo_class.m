classdef robo_class
    properties
        xy,orient,mapa,xy_rel,orient_rel,xy_inicial;
    end
    methods        
        function obj = robo_class(xy,orient,mapa)
            obj.xy = xy;
            obj.orient = orient;
            obj.mapa = 1;
            obj.xy_rel = xy;
            obj.xy_inicial = xy;
            obj.orient_rel = orient;
            obj.mapa = mapa;

        end
        %-----------------------------------------------------------------
        function [bool,saida] = verifica_linha(obj,i)
            bool = 0;
            xy = {};
            saida = obj.xy_rel;
            for ki = 1:length(obj.mapa(1,:))
                if(obj.mapa(i,ki)==0)
                    bool = 1;
                    xy = [xy,[i,ki]];
                end
            end
            if ~isempty(xy)  
                saida = xy{1};
                for m = 1:length(xy)                    
                    if distance(obj.xy_rel,saida)>distance(obj.xy_rel,xy{m})
                        saida = xy{m};
                    end
                end
            end
        end
        function obj = Mover_D(obj,mapaABS)
            obj.orient = obj.orient - pi/2;
            if not(obj.sensorObj(mapaABS))
                obj.xy = [obj.xy(1)+round(sin(obj.orient)) obj.xy(2)+round(cos(obj.orient))];
                
                obj.orient_rel = obj.orient_rel - pi/2;
                obj.xy_rel = [obj.xy_rel(1)+round(sin(obj.orient_rel)) obj.xy_rel(2)+round(cos(obj.orient_rel))];
            else
                obj.orient = obj.orient + pi/2;
                obj.orient_rel = obj.orient_rel + pi/2;
            end
            obj = obj.atualizaMapa(mapaABS);
        end
        function obj = Mover_E(obj,mapaABS)
            obj.orient = obj.orient + pi/2;
            if not(obj.sensorObj(mapaABS))
                obj.xy = [obj.xy(1)+round(sin(obj.orient)) obj.xy(2)+round(cos(obj.orient))];
                
                obj.orient_rel = obj.orient_rel + pi/2;
                obj.xy_rel = [obj.xy_rel(1)+round(sin(obj.orient_rel)) obj.xy_rel(2)+round(cos(obj.orient_rel))];
            else
                obj.orient = obj.orient - pi/2;
                obj.orient_rel = obj.orient_rel - pi/2;
            end
            obj = obj.atualizaMapa(mapaABS);
        end
        function obj = Mover_F(obj,mapaABS)
            if not(obj.sensorObj(mapaABS))
                obj.xy = [obj.xy(1)+round(sin(obj.orient)) obj.xy(2)+round(cos(obj.orient))];
                
                obj.xy_rel = [obj.xy_rel(1)+round(sin(obj.orient_rel)) obj.xy_rel(2)+round(cos(obj.orient_rel))];
            end
            obj = obj.atualizaMapa(mapaABS);
        end
        function obj = Mover_T(obj,mapaABS)
            if not(obj.sensorObj(mapaABS))
                obj.orient = obj.orient - pi;
                obj.xy = [obj.xy(1)+round(sin(obj.orient)) obj.xy(2)+round(cos(obj.orient))];
                
                obj.orient_rel = obj.orient_rel - pi;
                obj.xy_rel = [obj.xy_rel(1)+round(sin(obj.orient_rel)) obj.xy_rel(2)+round(cos(obj.orient_rel))];
            else
                obj.orient = obj.orient + pi;
                obj.orient_rel = obj.orient_rel + pi;
            end
            obj = obj.atualizaMapa(mapaABS);
        end
        %-----------------------------------------------------------------
        function obj = girar_D(obj,mapaABS)
            obj.orient = obj.orient - pi/2;
            
            obj.orient_rel = obj.orient_rel - pi/2;
            
            obj = obj.atualizaMapa(mapaABS);
        end
        function obj = girar_E(obj,mapaABS)
            obj.orient = obj.orient + pi/2;
            
            
            obj.orient_rel = obj.orient_rel + pi/2;
            
            obj = obj.atualizaMapa(mapaABS);
        end
        function obj = girar_M(obj,mapaABS)
            obj.orient = obj.orient + pi;
            
            obj.orient_rel = obj.orient_rel + pi;
            
            obj = obj.atualizaMapa(mapaABS);
        end
        %--------------------------------------------------------------
        function obstaculo = sensorObj(obj,mapaABS)
            olhar = [obj.xy(1)+round(sin(obj.orient)) obj.xy(2)+round(cos(obj.orient))];
            obstaculo = 1;
            if and(olhar(1)>0,olhar(2)>0)
                if and(olhar(1)<=length(mapaABS(:,1)),olhar(2)<=length(mapaABS(1,:)))
                    if mapaABS(olhar(1),olhar(2))~= 2
                        obstaculo = 0;
                    end
                end
            end
        end
        %--------------------------------------------------------------
        function desenha_robo(obj)
            hold on
            i = obj.xy(1)-1;
            j = obj.xy(2)-1;
            
            x = [j*10+2.5 j*10+7.5 j*10+7.5 j*10+2.5];
            y = [i*10+2.5 i*10+2.5 i*10+7.5 i*10+7.5];
            patch(x,y,[0 0 1])
            
            y = [i*10+5 i*10+2.5 i*10+7.5];
            x = [j*10+5 j*10+7.5 j*10+7.5];
            [x,y] = rot_esp(x,y,obj.orient,j*10+5,i*10+5) ;
            patch(x,y,[1 0 0])
            
            hold off;
        end
        function desenha_robo_rel(obj)
            hold on
            i = obj.xy_rel(1)-1;
            j = obj.xy_rel(2)-1;
            
            x = [j*10+2.5 j*10+7.5 j*10+7.5 j*10+2.5];
            y = [i*10+2.5 i*10+2.5 i*10+7.5 i*10+7.5];
            patch(x,y,[0 0 1])
            
            y = [i*10+5 i*10+2.5 i*10+7.5];
            x = [j*10+5 j*10+7.5 j*10+7.5];
            [x,y] = rot_esp(x,y,obj.orient_rel,j*10+5,i*10+5) ;
            patch(x,y,[1 0 0])            
            
            i = obj.xy_inicial(1)-1;
            j = obj.xy_inicial(2)-1;
            x = [j*10+2 j*10+6 j*10+6 j*10+8 j*10+6 j*10+6 j*10+2];
            y = [i*10+6.5 i*10+6.5 i*10+9 i*10+5 i*10+1 i*10+3.5 i*10+3.5];
            %[x,y] = rot_esp(x,y,obj.orient_rel,j*10+5,i*10+5) ;
            patch(x,y,[1 0 0])
             
            hold off;
        end
        %---------------------------------------------------------------
        function obj = atualizaMapa(obj,mapaABS)
            new_xy = [obj.xy_rel(1)+sin(obj.orient_rel) obj.xy_rel(2)+cos(obj.orient_rel)];
            if new_xy(1)<1
                %[abs(new_xy(1))+length(obj.mapa(:,1)+1),length(obj.mapa(1,:))]
                %deslocar aumentar abs(new_xy(1)) nas dimensoes
                auxiliar = zeros(abs(new_xy(1))+length(obj.mapa(:,1)+1),length(obj.mapa(1,:)));
                for i = 1:length(obj.mapa(:,1))
                    for j = 1:length(obj.mapa(1,:))
                        auxiliar(i+abs(new_xy(1))+1,j) = obj.mapa(i,j);
                    end
                end
                obj.mapa = auxiliar;
                obj.xy_rel(1) = obj.xy_rel(1) + abs(new_xy(1))+1;
                obj.xy_inicial(1) = obj.xy_inicial(1) + abs(new_xy(1))+1;
                new_xy(1)=1;
            end
            if new_xy(2)<1
                %deslocar aumentar abs(new_xy(2)) nas dimensoes
                %[length(obj.mapa(:,1)) abs(new_xy(2))+length(obj.mapa(1,:))+1]
                auxiliar = zeros(length(obj.mapa(:,1)),abs(new_xy(2))+length(obj.mapa(1,:))+1);
                for i = 1:length(obj.mapa(:,1))
                    for j = 1:length(obj.mapa(1,:))
                        auxiliar(i,j+abs(new_xy(2))+1) = obj.mapa(i,j);
                    end
                end
                obj.mapa = auxiliar;
                obj.xy_rel(2) = obj.xy_rel(2) + abs(new_xy(2))+1;
                obj.xy_inicial(2) = obj.xy_inicial(2) + abs(new_xy(2))+1;
                new_xy(2)=1;
            end
            if new_xy(1)>length(obj.mapa(:,1))
                auxiliar = zeros(abs(new_xy(1)),length(obj.mapa(1,:)));
                for i = 1:length(obj.mapa(:,1))
                    for j = 1:length(obj.mapa(1,:))
                        auxiliar(i,j) = obj.mapa(i,j);
                    end
                end
                obj.mapa = auxiliar;
            end
            if new_xy(2)>length(obj.mapa(1,:))
                auxiliar = zeros(length(obj.mapa(:,1)),abs(new_xy(2)));
                for i = 1:length(obj.mapa(:,1))
                    for j = 1:length(obj.mapa(1,:))
                        auxiliar(i,j) = obj.mapa(i,j);
                    end
                end
                obj.mapa = auxiliar;
            end
%             o_mpa = obj.mapa
            new_xy = round(new_xy);
%             %obj.mapa
            if obj.sensorObj(mapaABS) == 1
                obj.mapa(new_xy(1),new_xy(2)) = 2;
            else
                obj.mapa(new_xy(1),new_xy(2)) = 1;
            end
            figure(100);
            cla;
            desenha_mapa(obj.mapa)
            obj.desenha_robo_rel();
            figure(1)
            
        end
        function robo = moverF(robo,mapaABS)
            robo = robo.Mover_F(mapaABS);
            robo = robo.girar_D(mapaABS);
            robo = robo.girar_D(mapaABS);
            robo = robo.girar_D(mapaABS);
            robo = robo.girar_D(mapaABS);
            cla;
            desenha_mapa(mapaABS);
            robo.desenha_robo();
        end
        function robo = moverD(robo,mapaABS)
            robo = robo.girar_D(mapaABS);
            cla;
            desenha_mapa(mapaABS);
            robo.desenha_robo();
        end
    end
end
