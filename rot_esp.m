function [l1,l2] = rot_esp(dadosx,dadosy,rot,x,y)
    l1(length(dadosx)) = 0;
    l2(length(dadosx)) = 0;

    for n = 1:length(dadosx)
        dref = sqrt(power((dadosx(n)-x),2)+power((dadosy(n)-y),2));
        ang = atan2(dadosy(n)-y,dadosx(n)-x);
        l1(n) = x + dref*cos(rot+ang);
        l2(n) = y + dref*sin(rot+ang);
    end
end