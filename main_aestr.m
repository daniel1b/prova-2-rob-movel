clc; clear all;cla
mapaABS = zeros(12,12);
%0 para descconhecido
%1 para vazio
%2 para obstaculo
prob_obstaculo = 0.2;

%cria os obstaculos no mapa
for i = 1:length(mapaABS(:,1))
    for j = 1:length(mapaABS(1,:))
        if rand()>prob_obstaculo
            mapaABS(i,j) = 1;%livre
        else
            mapaABS(i,j) = 2;%obstaculo
        end
    end
end
for d = 1:length(mapaABS(:,1))
    mapaABS(d,1) = 2;
    mapaABS(d,length(mapaABS(1,:))) = 2;
end
for d = 1:length(mapaABS(1,:))
    mapaABS(1,d) = 2;
    mapaABS(length(mapaABS(:,1)),d) = 2;
end

%posicao inicial
pos_inicial = [randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)];
while mapaABS(pos_inicial(1),pos_inicial(2))==2
    pos_inicial = [randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)];
end
%posicao final
pos_final = [randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)];
while mapaABS(pos_final(1),pos_final(2))==2
    pos_final = [randi([1 length(mapaABS(:,1))],1,1) randi([1 length(mapaABS(:,1))],1,1)];
end

figure(1)
cla

desenha_mapa(mapaABS);
hold on
i = pos_final(1);
j = pos_final(2);
patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 0]);
hold off

figure(2)
vetor = rota_aestr_v2(mapaABS,pos_inicial,pos_final,100,0);
cla
desenha_mapa(mapaABS)
hold on
for d =1:length(vetor)    
i = vetor{d}(1);
j = vetor{d}(2);
patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 0 1]);
end
i = pos_final(1);
j = pos_final(2);
patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 1]);
i = pos_inicial(1);
j = pos_inicial(2);
patch([(j-1)*10 j*10 j*10 (j-1)*10],[(i-1)*10 (i-1)*10 i*10 i*10], [0 1 0]);
hold off
figure(1)

robo = robo_class(pos_inicial,0,mapaABS);
robo.desenha_robo();
deltat = 0.1;
for i = 2:length(vetor)    
    girar = robo.orient;
    count = 0;
    cel = [vetor{i-1}(1)+round(sin(girar)) vetor{i-1}(2)+round(cos(girar))];
    while cel(1)~=vetor{i}(1)||cel(2)~=vetor{i}(2)
        count = count+1;
        girar = girar + pi/2;
        cel = [vetor{i-1}(1)+round(sin(girar)) vetor{i-1}(2)+round(cos(girar))];
    end
    

    if count == 1
        robo = robo.girar_E(mapaABS);
        robo.desenha_robo();
        pause(deltat)
    elseif count == 2
        robo = robo.girar_D(mapaABS);
        robo.desenha_robo();
        pause(deltat)
        robo = robo.girar_D(mapaABS);
        robo.desenha_robo();
        pause(deltat)
    elseif count == 3 
        robo = robo.girar_D(mapaABS);
        robo.desenha_robo();
        pause(deltat)
    end
    robo = robo.Mover_F(mapaABS);
    
        cla
    desenha_mapa(mapaABS);
    hold on
    k = pos_final(1);
    j = pos_final(2);
    patch([(j-1)*10 j*10 j*10 (j-1)*10],[(k-1)*10 (k-1)*10 k*10 k*10], [0 1 0]);
    k = pos_inicial(1);
    j = pos_inicial(2);
    patch([(j-1)*10 j*10 j*10 (j-1)*10],[(k-1)*10 (k-1)*10 k*10 k*10], [1 0 1]);
    hold off
    robo.desenha_robo();
    pause(deltat)
end


